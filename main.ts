function Greeter(saludo:string) {
    this.saludo = saludo;
}

Greeter.prototype.decirHola = function () {
    return 'Hola ' + this.saludo;
}

let greeter = new Greeter('mundo');

let boton = document.createElement('button');
boton.textContent = 'Saludar'

boton.onclick = function () {
    alert(greeter.decirHola());
}

document.body.appendChild(boton);
