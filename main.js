function Greeter(saludo) {
    this.saludo = saludo;
}
Greeter.prototype.decirHola = function () {
    return 'Hola ' + this.saludo;
};
var greeter = new Greeter('mundo');
var boton = document.createElement('button');
boton.textContent = 'Saludar';
boton.onclick = function () {
    alert(greeter.decirHola());
};
document.body.appendChild(boton);
